


Feature: Login In order to use the app the user must be able to Login

  Background:

    Given  the user is on the login page page

  Scenario Outline: Login Success

    When User pass data on Invalid "<Email>" and "<Password>" on login page

    And   Click on login

    Then  User should not able to login and see "<ErrorMessage>"

       Examples:

      | Email               | Password     |  ErrorMessage             |

      |  test12@drugdev.com | supersecret  |  Credentials are incorrect  |



