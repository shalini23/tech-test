package com.Base;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.apache.commons.io.FileUtils.copyFile;

public class Hooks extends BasePage{

    BrowserSelector browserSelector = new BrowserSelector();
    static String timeStamp = new SimpleDateFormat("dd.MM.yy.HH.mm.ss").format(new Date());

    @Before
    public void setUpBrowser()
    {browserSelector.setUpBrowser();
    driver.manage().timeouts().implicitlyWait(20, TimeUnit.MILLISECONDS);
    driver.manage().window().fullscreen();
    driver.get("https://sprinkle-burn.glitch.me/");
    }
    @After
    public void closeBrowser(Scenario scenario) throws IOException{
        if(scenario.isFailed()) {
            String sceenshotName = scenario.getName().replaceAll("[.,;:?!]", "") + timeStamp + ".png";
            try {
                File sourcePath = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                File destinationPath = new File(System.getProperty("user.dir") + "/target/cucumber-reports/extent-reports/screenshot/" + sceenshotName);
                copyFile(sourcePath, destinationPath);
                scenario.write("!!!...Scenario Failed...!!!!!Please see attached screenshot for the error/issue");
                scenario.embed(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES), "image/png");
            } catch (IOException e) {
            }
        } driver.quit();
        }
    }

