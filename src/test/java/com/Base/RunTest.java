package com.Base;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = ".", tags = "@Valid_login,@Invalid_Login",glue = "",
                 format = {"pretty","html:target/cucumber-reports"})

public class RunTest {

}
