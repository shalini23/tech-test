package com.Base;

import PageObjects.Login_Page;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;

public class MyStepdefs extends BasePage
{
    Login_Page login_page = new Login_Page();

    @Given("^the user is on login page$")
    public void the_user_is_on_login_page() {
        login_page.assert_LoginPage();
    }

    @When("^user enter valid credentials$")
    public void user_enter_valid_credentials()  {
        login_page.enter_Valid_email();

    }


    @When("^clicks Login$")
    public void clicks_Login() throws InterruptedException {
     login_page.click_login();
    }

    @Then("^the user is presented with a welcome message$")
    public void the_user_is_presented_with_a_welcome_message() {
       login_page.success_Login_Message();
    }

    @Given("^the user is on the login page$")
    public void the_user_is_on_the_login_page()  {
        login_page.assert_LoginPage();
    }

    @When("^The user enters wrong credential$")
    public void the_user_enters_wrong_credential() {
        login_page.enter_InValid_Credentials();
    }

    @And("^Clicks Login$")
    public void click_Login() throws InterruptedException {
        login_page.click_login();

    }

    @Then("^The user is presented with a error message$")
    public void the_user_is_presented_with_the_error_message()  {
        login_page.error_Login_Message();

    }










}
