package com.Base;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

/* Here reference variable of type WebDriver allows us to assign the driver object to
 different browser specific drivers. Thus allowing multi-browser testing by
 assigning the driver object to any of the desired browser.
shareimprove this answer

 */
public class BrowserSelector extends Utils{

    LoadProp loadProperties = new LoadProp();

    public void setUpBrowser()
    {
        String browser = loadProperties.getProperty("browser");

        if (browser.equalsIgnoreCase("chrome")){
            System.setProperty("webdriver.chrome.driver","src\\test\\Resources\\BrowserDirectory\\chromedriver.exe");
            ChromeOptions options = new ChromeOptions();
            options.addArguments("disable-infobars");
            options.addArguments("--disable-extensions");
            options.addArguments("--disable-setUpBrowser-side-navigation");
            options.addArguments("--incognito");
//            options.addArguments("Headless");
            options.addArguments("--disable-blink-features=BlockCredentialedSubresources");
            driver = new ChromeDriver(options);
           driver.manage().window().maximize();
        }
       else {
            System.out.println("Browser name is empty or typed wrong" + browser);
        }
    }
}
