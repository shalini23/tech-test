package com.Base;


import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import static jdk.nashorn.internal.runtime.regexp.joni.Config.log;
import static org.apache.commons.io.FileUtils.copyFile;

public class
Utils extends BasePage {


    // 1 method for getting text from web
    public String get1TextFromElement(By by) {
        String actualText = driver.findElement(by).getText();
        System.out.println(actualText);
        return actualText;
    }

    //2 Get text from Element

    public static String getTextFromElement(By by) {
        return driver.findElement(by).getAttribute("innerHTML");
    }

    // 3   Assert Text Message

    public static void assertWithText(String expected, By by) {
        String actual = getTextFromElement(by);
        System.out.println(actual);
        Assert.assertEquals(expected, actual);
    }


    // 4- To assert user is on right url
    public static void assertCurrentURL(String expectedUrl) {
        //String url = driver.getCurrentUrl();
        Assert.assertEquals(expectedUrl, driver.getCurrentUrl());
    }


    //5- Method to get screenshot of browser
    public static void browserScreentshot() {
        try {
            File source = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            copyFile(source, new File("src\\Screenshotscreenshot.png"));
        } catch (IOException ex) {
            System.out.println("There is a problem in taking a screenshot");
        }
    }

    // 6- method for Enter Keys

    public void enterText(By by, String name) {
        driver.findElement(by).sendKeys(name);
    }

    //7- Method for Click

    public void clickElement(By by) {
        driver.findElement(by).click();
    }


    // 8- select value from dropdown menu
    public static void enterTextByName(By by, String name) {
        Select select = new Select(driver.findElement(by));
        select.selectByVisibleText(name);

    }


    // 9 - Wait for Locator to be visible for given time in seconds
    public static void waitForElementToVisible(By by, int time)
    {
        WebDriverWait wait = new WebDriverWait(driver , time);
        wait.until(ExpectedConditions.presenceOfElementLocated(by));

    }

}








