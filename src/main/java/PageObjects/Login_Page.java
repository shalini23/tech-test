package PageObjects;


import com.Base.LoadProp;
import com.Base.Utils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import sun.awt.geom.AreaOp;

import java.util.concurrent.TimeUnit;


public class Login_Page extends Utils {

    LoadProp loadProp = new LoadProp();//load properties from config file

    //make all locators in this class so no other class can access it
    private By _Email = By.xpath("//input[@name='email']");

    private By _Password = By.xpath("//input[@name='password']");

    private By _Login = By.xpath("//button[contains(@class,'dim bn')]");

    private By _LoginSuccessMessage = By.xpath("//article[@class='pa4 black-80 content']");


    //Assert LoginPage

    public void assert_LoginPage() {
        assertCurrentURL("https://sprinkle-burn.glitch.me/");
    }


    //passing valid credentials for both email and password field on login Page

    public void enter_Valid_email() {
        enterText(_Email,loadProp.getProperty("email"));
        enterText(_Password, loadProp.getProperty("password"));

    }


    public void click_login() throws InterruptedException {
        clickElement(_Login);

       Thread.sleep(2000);// wait for element to be visible
    }


    public void success_Login_Message()
    {
         //get text from the element
        System.out.println(driver.findElement(By.xpath("//article[@class='pa4 black-80 content']")).getText());

        //assert it with text driver getting from the locator
        Assert.assertTrue(driver.findElement(By.xpath("//article[@class='pa4 black-80 content']")).getText().
                contains("Welcome Dr I Test"));
    }



    public void enter_InValid_Credentials()
    {

        //send input data through config properties
        enterText(_Email,loadProp.getProperty("invalidEmail") );

        enterText(_Password,loadProp.getProperty("invalidPassword"));

    }



    public void error_Login_Message()
    {
        //get text from the element
        System.out.println(driver.findElement(By.xpath("//div[@class='pa2 mb2 bg-red white']")).getText());

        //assert it with text driver getting from the locator
        Assert.assertTrue(driver.findElement(By.xpath("//div[@class='pa2 mb2 bg-red white']")).getText().
                contains("Credentials are incorrect"));
    }
}
