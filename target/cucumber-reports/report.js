$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/Resources/Features/invalid_login.feature");
formatter.feature({
  "line": 5,
  "name": "Login",
  "description": "",
  "id": "login",
  "keyword": "Feature",
  "tags": [
    {
      "line": 2,
      "name": "@Invalid_Login"
    }
  ]
});
formatter.before({
  "duration": 5837599403,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "User should not able to login with invalid credential",
  "description": "",
  "id": "login;user-should-not-able-to-login-with-invalid-credential",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 10,
  "name": "the user is on the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "The user enters wrong credential",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "Clicks Login",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "The user is presented with a error message",
  "keyword": "Then "
});
formatter.match({
  "location": "MyStepdefs.the_user_is_on_the_login_page()"
});
formatter.result({
  "duration": 151844561,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.the_user_enters_wrong_credential()"
});
formatter.result({
  "duration": 240619172,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.click_Login()"
});
formatter.result({
  "duration": 2069328905,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.the_user_is_presented_with_the_error_message()"
});
formatter.result({
  "duration": 60982446,
  "status": "passed"
});
formatter.after({
  "duration": 684215683,
  "status": "passed"
});
formatter.uri("src/test/Resources/Features/valid_login.feature");
formatter.feature({
  "line": 3,
  "name": "Login In order to use the app the user must be able to Login",
  "description": "",
  "id": "login-in-order-to-use-the-app-the-user-must-be-able-to-login",
  "keyword": "Feature",
  "tags": [
    {
      "line": 2,
      "name": "@Valid_login"
    }
  ]
});
formatter.before({
  "duration": 4427521073,
  "status": "passed"
});
formatter.scenario({
  "comments": [
    {
      "line": 5,
      "value": "#valid login Scenario"
    }
  ],
  "line": 6,
  "name": "Login Success",
  "description": "",
  "id": "login-in-order-to-use-the-app-the-user-must-be-able-to-login;login-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 7,
  "name": "the user is on login page",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "user enter valid credentials",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "clicks Login",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "the user is presented with a welcome message",
  "keyword": "Then "
});
formatter.match({
  "location": "MyStepdefs.the_user_is_on_login_page()"
});
formatter.result({
  "duration": 7405819,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.user_enter_valid_credentials()"
});
formatter.result({
  "duration": 230362196,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.clicks_Login()"
});
formatter.result({
  "duration": 2070719126,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.the_user_is_presented_with_a_welcome_message()"
});
formatter.result({
  "duration": 56857612,
  "status": "passed"
});
formatter.after({
  "duration": 671000883,
  "status": "passed"
});
});