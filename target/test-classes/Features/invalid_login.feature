
@Invalid_Login


Feature: Login

  Scenario: User should not able to login with invalid credential


    Given the user is on the login page
    When The user enters wrong credential
    And Clicks Login
    Then The user is presented with a error message

